## Rust CI gitlab integration

## Images

### Base Ubuntu image

[![](https://images.microbadger.com/badges/version/rustdocker/ubuntu.svg)](https://microbadger.com/images/rustdocker/ubuntu "Ubuntu Xenial")
[![](https://images.microbadger.com/badges/image/rustdocker/ubuntu.svg)](https://microbadger.com/images/rustdocker/ubuntu "Ubuntu Xenial")

### Bare rustup (no Rust compiler itself)
[![](https://images.microbadger.com/badges/version/rustdocker/rustup.svg)](http://microbadger.com/images/rustdocker/rustup "rustup")
[![](https://images.microbadger.com/badges/image/rustdocker/rustup.svg)](http://microbadger.com/images/rustdocker/rustup "rustup")  

### Rust images
[![](https://images.microbadger.com/badges/version/rustdocker/rust:stable.svg)](http://microbadger.com/images/rustdocker/rust:stable "Rust 1.63.0")
[![](https://images.microbadger.com/badges/image/rustdocker/rust:stable.svg)](http://microbadger.com/images/rustdocker/rust:stable "Rust 1.63.0")  
[![](https://images.microbadger.com/badges/version/rustdocker/rust:beta.svg)](http://microbadger.com/images/rustdocker/rust:beta "Rust beta")
[![](https://images.microbadger.com/badges/image/rustdocker/rust:beta.svg)](http://microbadger.com/images/rustdocker/rust:beta "Rust beta")  
[![](https://images.microbadger.com/badges/version/rustdocker/rust:nightly.svg)](http://microbadger.com/images/rustdocker/rust:nightly "Rust nightly")
[![](https://images.microbadger.com/badges/image/rustdocker/rust:nightly.svg)](http://microbadger.com/images/rustdocker/rust:nightly "Rust nightly")

### Rust images with pre-installed rustfmt
[![](https://images.microbadger.com/badges/version/rustdocker/rustfmt:stable.svg)](https://microbadger.com/images/rustdocker/rustfmt:stable "Rust 1.63.0")
[![](https://images.microbadger.com/badges/image/rustdocker/rustfmt:stable.svg)](http://microbadger.com/images/rustdocker/rustfmt:stable "Rust 1.63.0")  
[![](https://images.microbadger.com/badges/version/rustdocker/rustfmt:beta.svg)](http://microbadger.com/images/rustdocker/rustfmt:beta "Rust beta")
[![](https://images.microbadger.com/badges/image/rustdocker/rustfmt:beta.svg)](http://microbadger.com/images/rustdocker/rustfmt:beta "Rust beta")  
[![](https://images.microbadger.com/badges/version/rustdocker/rustfmt:nightly.svg)](http://microbadger.com/images/rustdocker/rustfmt:nightly "Rust nightly")
[![](https://images.microbadger.com/badges/image/rustdocker/rustfmt:nightly.svg)](http://microbadger.com/images/rustdocker/rust:nightly "Rust nightly")

### Rust images with pre-installed clippy
[![](https://images.microbadger.com/badges/version/rustdocker/rustfmt_clippy:nightly.svg)](https://microbadger.com/images/rustdocker/rustfmt_clippy:nightly "Rust nightly with clippy")
[![](https://images.microbadger.com/badges/image/rustdocker/rustfmt_clippy:nightly.svg)](https://microbadger.com/images/rustdocker/rustfmt_clippy:nightly "Rust nightly with clippy") 

### .gitlab-ci.yml example

```
.cargo_test_template: &cargo_test
  stage: test
  script:
    - cargo test --verbose --jobs 1 --all

stages:
  - test
  - deploy

stable:cargo:
  image: rustdocker/rust:stable
  <<: *cargo_test

beta:cargo:
  image: rustdocker/rust:beta
  <<: *cargo_test

nightly:cargo:
  image: rustdocker/rust:nightly
  <<: *cargo_test

.pages:
  image: rustdocker/rust:stable
  stage: deploy
  only:
    - master
  script:
    - cargo doc
    - rm -rf public
    - mkdir public
    - cp -R target/doc/* public
  artifacts:
    paths:
    - public
```
