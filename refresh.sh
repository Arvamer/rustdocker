#!/bin/bash

UBUNTU="rustdocker/ubuntu"
BUILD_DATE=$(date -u +"%Y%m%d")
RUSTUP="rustdocker/rustup"
RUST="rustdocker/rust"
RUSTFMT="rustdocker/rustfmt"
RUSTFMT_CLIPPY="rustdocker/rustfmt_clippy"
TAGS="1.23.0 beta nightly"
STABLE="1.23.0"

UBUNTU_WEBHOOK="https://hooks.microbadger.com/images/rustdocker/ubuntu/xeiqcYeoRQ24sWAXByp6jELp5lo="
RUSTUP_WEBHOOK="https://hooks.microbadger.com/images/rustdocker/rustup/DS5WmL_71NAyEK_vzYrPv1EeM7Q="
RUST_WEBHOOK="https://hooks.microbadger.com/images/rustdocker/rust/z35rD0wve1kbFEXQgRXAvf99pW0="
RUSTFMT_WEBHOOK="https://hooks.microbadger.com/images/rustdocker/rustfmt/QngrBtgcUQ4ysgbjkxekwDxctY0="
CLIPPY_WEBHOOK="https://hooks.microbadger.com/images/rustdocker/rustfmt_clippy/kMOKGoR4-jxvwBL5Nwzcqq-48UA="
WEBHOOKS="$UBUNTU_WEBHOOK $RUSTUP_WEBHOOK $RUST_WEBHOOK $RUSTFMT_WEBHOOK $CLIPPY_WEBHOOK"

docker build --pull --build-arg BUILD_DATE=$BUILD_DATE -t $UBUNTU:$BUILD_DATE ubuntu-dev
docker tag $UBUNTU:$BUILD_DATE $UBUNTU:latest

docker build --build-arg BUILD_DATE=$BUILD_DATE -t $RUSTUP:$BUILD_DATE rustup
docker tag $RUSTUP:$BUILD_DATE $RUSTUP:latest

for tag in $TAGS
do
	docker build --build-arg BUILD_DATE=$BUILD_DATE --build-arg RUST_VERSION=$tag -t $RUST:$tag rust
	docker build --build-arg BUILD_DATE=$BUILD_DATE --build-arg RUST_VERSION=$tag -t $RUSTFMT:$tag rustfmt/$tag
done

docker build --build-arg BUILD_DATE=$BUILD_DATE --build-arg RUST_VERSION=nightly -t $RUSTFMT_CLIPPY:nightly rustfmt_clippy/nightly

docker tag $RUST:$STABLE $RUST:stable
docker tag $RUSTFMT:$STABLE $RUSTFMT:stable

# push images to repo
docker push $UBUNTU:$BUILD_DATE
docker push $UBUNTU:latest
docker push $RUSTUP:$BUILD_DATE
docker push $RUSTUP:latest

for tag in $TAGS
do
	docker push $RUST:$tag
	docker push $RUSTFMT:$tag
done
docker push $RUSTFMT_CLIPPY:nightly

docker push $RUST:stable
docker push $RUSTFMT:stable

for webhook in $WEBHOOKS
do
	curl -X POST $webhook
done

# cleanup
docker rmi $RUSTFMT:stable $RUST:stable
for tag in $TAGS
do
	docker rmi $RUSTFMT:$tag $RUST:$tag
done
docker rmi $RUSTFMT_CLIPPY:nightly

docker rmi $RUSTUP:latest $RUSTUP:$BUILD_DATE
docker rmi $UBUNTU:latest $UBUNTU:$BUILD_DATE
